#include <iostream>
#include <WinSock2.h>
#include <string>
#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable:4996)

#define PORT 55555

SOCKET Connection;
//ClientHandler function without parametr
void ClientHandler() {
	int msg_size;
	
	while (true) {
		//get message size
		int recv_status = recv(Connection, (char*)&msg_size, sizeof(int), NULL);
		char* msg = new char[msg_size + 1];
		msg[msg_size] = '\0';
		//get message
		recv_status =recv(Connection, msg, msg_size, NULL);
		std::cout << msg << std::endl;

		delete[] msg;

		if (recv_status == SOCKET_ERROR) {
			break;
		}
	}
}

int main() {
	//initialize WinSock
	WSADATA wsaData;
	WORD DLLVesrsion = MAKEWORD(2, 1);
	//check winsock started or no
	if (WSAStartup(DLLVesrsion, &wsaData) != 0) {
		std::cerr << "Error" << std::endl;
		exit(1);
	}
	//config winsock address
	SOCKADDR_IN addr;
	int sizeofaddr = sizeof(addr);
	//addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	//addr.sin_addr.s_addr = inet_addr("172.16.255.26");
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;

	//new socket creating
	Connection = socket(AF_INET, SOCK_STREAM, NULL);
	//connecting with server
	if (connect(Connection, (SOCKADDR*)&addr, sizeof(addr)) != 0) {
		std::cerr << "Error: failed connect to server.\n";
		return 1;
	}
	std::cout << "Connected!\n";
	/*char msg[256];
	recv(Connection, msg, sizeof(msg), NULL);
	std::cout << msg << std::endl;*/

	//creates a new thread for the process
	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, NULL, NULL, NULL);
	std::string msg1;
	while (true) {
		//cin message
		std::getline(std::cin, msg1);
		int msg_size = msg1.size();
		//send message size
		if (msg_size > 30) {
			exit(1);
			return 1;
		}
		send(Connection, (char*)&msg_size, sizeof(int), NULL);
		//send message
		send(Connection, msg1.c_str(), msg_size, NULL);
		Sleep(10);
	}
	return 0;
}
