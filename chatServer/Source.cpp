#include <iostream>
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib")
#pragma warning(disable:4996)

#define PORT 55555

SOCKET Connection[100];
int Counter = 0;
//ClientHandler function with index of client who sended message;
void ClientHandler(int index) {
	int msg_size;
	while (true) {
		//get the size
		int recv_status =recv(Connection[index], (char*)&msg_size, sizeof(int), NULL);
		char* msg = new char[msg_size + 1];
		msg[msg_size] = '\0';
		//get the message
		recv_status =recv(Connection[index], msg, msg_size, NULL);
		//loop(from 0 to client's counter
		for (int i = 0; i < Counter; i++) {
			if (i == index) {
				continue;
			}
			if (msg_size > 30) {
				exit(1);
				return;
			}
			//send the size
			send(Connection[i], (char*)&msg_size, sizeof(int), NULL);
			//send the message
			send(Connection[i], msg, msg_size, NULL);
		}
		delete[]msg;
		if (recv_status == -1) {
			return;
		}
	}
}

int main() {
	//Initialize WinSock
	WSADATA wsaData;
	WORD DLLVesrsion = MAKEWORD(2, 1);
	//Check WinSock starting
	if (WSAStartup(DLLVesrsion, &wsaData) != 0) {
		std::cerr << "Error" << std::endl;
		exit(1);
	}
	//Config WinSock address
	SOCKADDR_IN addr;
	int sizeofaddr = sizeof(addr);
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;
	//creat Socket sListen
	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL);
	//bind socket and address
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr));
	//listen socket: SOMAXCONN is maximum count of clients
	listen(sListen, SOMAXCONN);

	SOCKET newConnection;
	for (int i = 0; i < 100; i++) {
		//connection request from a client
		newConnection = accept(sListen, (SOCKADDR*)&addr, &sizeofaddr);

		if (newConnection == 0) {
			std::cerr << "Error #2\n";
		}
		else {
			std::cout << "Client Connected \n";
			std::string msg = "Hello, It's my first chat program!";
			int msg_size = msg.size();
			//send message size
			send(newConnection, (char*)&msg_size, sizeof(int), NULL);
			//send message 
			send(newConnection, msg.c_str(), msg_size, NULL);
			Connection[i] = newConnection;
			Counter++;
			//creates a new thread for the process
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, (LPVOID)(i), NULL, NULL);
		}
	}
	return 0;
}